# Specify which docker with nodeJS version you want
FROM node:12

# Create working directory
WORKDIR /usr/src/softeng_docker_hazi

# Copy dependencies file
COPY package*.json ./
# and install the dependencies (without the development deps)
RUN npm install --production

# Copy all the directories which are part of the application (see .dockerignore)
COPY . .

# The port 3000 is hardcoded for the server so we need to expose that port
# expose = The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime.
# see https://docs.docker.com/engine/reference/builder/
EXPOSE 8080

# Start the application using this command
CMD ["node", "index.js"]